#!/usr/bin/env python3
import os
import random
import shutil
import sys

import xlsxwriter

from options import options, free_space_options, names

# leave one buffer col, row
CELLS = [(col, row) for col in range(1,6) for row in range(1,6)]


def set_width_height(worksheet):
    # these arent the same units and I want them basically the same
    width = 25
    height = 130
    # set both col and row width/height to size for first 5 row/col
    worksheet.set_column(1, 5, width)
    for row in range(1,6):
        worksheet.set_row(row, height)


def create_one_card(options, free_space_options, output_file_name):
    workbook = xlsxwriter.Workbook(output_file_name)
    worksheet = workbook.add_worksheet()

    set_width_height(worksheet)

    formatter = workbook.add_format()
    formatter.set_font_size(20)
    formatter.set_align("center")
    formatter.set_align("vcenter")
    formatter.set_text_wrap()
    formatter.set_border(2)

    random.shuffle(options)
    for i, cell in enumerate(CELLS):
        col, row = cell
        if row == 3 and col == 3:
            # center square
            message = random.choice(free_space_options) + "\n(FREE SPACE)"
            worksheet.write_string(row, col, message, formatter)
        else:
            worksheet.write_string(row, col, options[i], formatter)
    workbook.close()


def main():
    output_dir = "cards"
    shutil.rmtree(output_dir)
    os.mkdir(output_dir)
    for name in names:
        out_file_path = os.path.join(output_dir, f"{name}.xlsx")
        create_one_card(options, free_space_options, out_file_path)


if __name__ == '__main__':
    main()
